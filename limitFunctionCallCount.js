// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned

function limitFunctionCallCount(cb, n) {
    let count = 0;

    function invoke() {
        if (count < n) {
            count++
            cb()
            return null
        } else {
            console.log(`Callback limit reached :${n}-times count`);
            return false;
        }
    }
    return { invoke };
}

module.exports = limitFunctionCallCount;