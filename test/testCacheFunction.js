const cacheFunction = require("../cacheFunction");

let cb = () => console.log("argument added to cache");

let returnedFunction = cacheFunction(cb);

console.log(returnedFunction(1));
console.log(returnedFunction(2));
console.log(returnedFunction(1, 2));
console.log(returnedFunction(1, 2, 3, 4, 5));
console.log(returnedFunction(1, 2));
console.log(returnedFunction(2, 1));
