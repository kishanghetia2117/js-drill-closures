const limitFunctionCallCount = require("../limitFunctionCallCount.js")

let count = 0
let cb = () => {
    count++
    console.log("Callback called : " + count)
};
let n = 7;

let nCount = limitFunctionCallCount(cb, n);

for (index = 0; index <= 10; index++) {
    let result = nCount.invoke();

    if (result === false) {
        break;
    }
}
